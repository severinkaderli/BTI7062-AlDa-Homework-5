# BTI7062-AlDa-Homework-5
## Task
Reprogram Mergesort by
1. implementing our single- & multi-threaded DnC-interfaces
2. using Insertion Sort as
    1. base function (determine best input size for the base case)
    2. merge function (use a functional-interface parameter)
	
and compare the resulting performances graphically

## ZIP File
The ready to submit zip file can be found [here](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-5/builds/artifacts/master/raw/TEAM-Kaderli-Kilic-Schaer.zip?job=deploy).
