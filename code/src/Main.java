import algorithms.mergesort.MergeSortDNC;
import algorithms.mergesort.MergeSortThreadedDNC;
import algorithms.utils.Config;
import algorithms.utils.DurationTimer;
import algorithms.utils.DataProvider;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle(createTitle());
        XYChart<Number, Number> chart = getChart();
        Scene scene = new Scene(chart, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();


        for (XYChart.Series<Number, Number> s : chart.getData()) {
            float avgms = 0;
            for (XYChart.Data<Number, Number> d : s.getData()) {
                Tooltip tooltip = new Tooltip(String.format("Sorted %d elements in %.2fms%n%s", d.getXValue().intValue(), d.getYValue().doubleValue(), s.getName()));
                Tooltip.install(d.getNode(), tooltip);
                avgms += d.getYValue().floatValue();
            }
            avgms /= s.getData().size();
            s.setName(s.getName() + String.format("(∅ %.2fms)", avgms));
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    private static String createTitle(){
        return String.format("Merge Sort DNC Comparison (case = %s, base = %s, merge = %s)",
                Config.BENCHMARK_CASE.name(),
                Config.STRATEGIES.baseCase().toString(),
                Config.STRATEGIES.recombineStrategy().toString());
    }

    private static XYChart<Number, Number> getChart() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Number of sorted elements");
        xAxis.setAutoRanging(false);
        xAxis.setLowerBound(0);
        xAxis.setUpperBound(Config.UPPER_BOUND);
        xAxis.setTickUnit(1.0);

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Execution time [ms]");

        for (int i = 0; i < Config.NUMBER_OF_WARMUP_ITERATIONS; i++) {
            System.out.println("Warmup Iteration #" + (i + 1) + " of " + Config.NUMBER_OF_WARMUP_ITERATIONS);
            getSimpleSeries();
            getJavaSeries();
            getThreadedSeries();
        }

        System.out.println("Running benchmark now:");

        LineChart<Number, Number> chart = new LineChart<>(xAxis, yAxis);
        chart.setTitle(createTitle());
        chart.getData().add(smooth(getSimpleSeries()));
        chart.getData().add(smooth(getJavaSeries()));
        chart.getData().addAll(getThreadedSeries().stream().map(Main::smooth).collect(Collectors.toList()));

        return chart;
    }

    private static Series<Number, Number> smooth(Series<Number, Number> input){
        if(!Config.APPLY_SMOOTHING){
            return input;
        }
        System.out.println("\t\tApplying smoothing with a factor of " + Config.SMOOTHING_FACTOR + "...");
        Series<Number, Number> output = new Series<>();
        for(int i = 0; i < input.getData().size(); i++){
            if(i < Config.SMOOTHING_FACTOR){
                output.getData().add(input.getData().get(i));
            }else{
                double y = 0;

                for(int j = Config.SMOOTHING_FACTOR; j >= 0; j--){
                    y += input.getData().get(i - j).getYValue().doubleValue();
                }
                Number yValue = y / Config.SMOOTHING_FACTOR;

                output.getData().add(new XYChart.Data<>(input.getData().get(i).getXValue(), yValue));
            }
        }
        output.setName(input.getName());
        return output;
    }

    private static Series getSimpleSeries() {
        System.out.println("\tCalculating with simple DnC programming...");
        DurationTimer timer = new DurationTimer();
        Series series = new Series();
        series.setName("Simple DNC");
        for (int i = 0; i <= Config.UPPER_BOUND; i += Config.STEP) {
            timer.start();
            MergeSortDNC sort = new MergeSortDNC(DataProvider.get(i), Config.STRATEGIES);
            check(sort.divideAndConquer());
            timer.stop();
            series.getData().add(new XYChart.Data(i, timer.getDuration()));
        }

        return series;
    }

    private static Series getJavaSeries() {
        System.out.println("\tCalculating with java sorting algorithm...");
        DurationTimer timer = new DurationTimer();
        Series series = new Series();
        series.setName("Sorting with Java");
        for (int i = 0; i <= Config.UPPER_BOUND; i += Config.STEP) {
            timer.start();
            List<Long> sort = DataProvider.get(i);
            Collections.sort(sort);
            timer.stop();
            series.getData().add(new XYChart.Data(i, timer.getDuration()));
        }

        return series;
    }

    private static List<Series<Number, Number>> getThreadedSeries() {
        List<Series<Number, Number>> allSeries = new ArrayList<>();

        for (int threadCount : Config.THREAD_COUNTS) {
            ForkJoinPool pool = new ForkJoinPool(threadCount);

            System.out.printf("\tCalculating with %d threads...%n", threadCount);
            DurationTimer timer = new DurationTimer();
            Series series = new Series();
            series.setName(String.format("Threaded DNC (%d threads)", threadCount));
            for (int i = 0; i <= Config.UPPER_BOUND; i += Config.STEP) {
                timer.start();
                MergeSortThreadedDNC sort = new MergeSortThreadedDNC(new Vector<>(DataProvider.get(i)), Config.STRATEGIES);
                check(sort.divideAndConquer(pool));
                timer.stop();
                series.getData().add(new XYChart.Data(i, timer.getDuration()));
            }
            allSeries.add(series);
        }
        return allSeries;
    }

    private static void check(List<Long> result){
        if(Config.CHECK_CORRECTNESS){
            System.out.printf("\t\tChecking results of length %d for correctness...", result.size());
            List<Long> sorted = new ArrayList<>(result);
            Collections.sort(sorted);
            if(!sorted.equals(result)){
                throw new IllegalArgumentException("not actually sorted!");
            }
            System.out.println(" OK!");
        }
    }

}
