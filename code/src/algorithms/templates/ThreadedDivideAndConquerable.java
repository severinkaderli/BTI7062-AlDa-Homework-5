package algorithms.templates;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.*;

/**
 * Sub interface which uses thread pools to recycle threads.
 */

public interface ThreadedDivideAndConquerable<OutputType> extends DivideAndConquerable<OutputType> {

    List<? extends ThreadedDivideAndConquerable<OutputType>> decompose();

    default OutputType divideAndConquer(ForkJoinPool pool) {

        if (this.isBasic()) {
            return this.baseFun();
        }

        List<? extends ThreadedDivideAndConquerable<OutputType>> subcomponents = this.decompose();

        List<OutputType> intermediateResults = new Vector<>(subcomponents.size());

        // threadable if the input number is high enough and the pool has threads left for execution
        if (isThreadable() && pool.getActiveThreadCount() < pool.getParallelism()) {
            List<ForkJoinTask<Boolean>> tasks = new Vector<>(subcomponents.size());
            // submit all the tasks
            for (ThreadedDivideAndConquerable<OutputType> subcomponent : subcomponents) {
                tasks.add(pool.submit(() -> intermediateResults.add(subcomponent.divideAndConquer(pool))));
            }

            try {
                // execute all the tasks
                for (ForkJoinTask<Boolean> task : tasks) {
                    task.get();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            // the given input is not suitable for threading, we execute it sequentially
            subcomponents.forEach(subcomponent -> {
                ThreadedDivideAndConquerable<OutputType> s = subcomponent;
                intermediateResults.add(s.divideAndConquer(pool));
            });

        }
        return recombine(intermediateResults);
    }

    boolean isThreadable();
}
