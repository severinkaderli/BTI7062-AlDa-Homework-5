package algorithms.mergesort;

import algorithms.mergesort.strategies.BaseCaseStrategy;
import algorithms.mergesort.strategies.InsertionSortBaseCaseStrategy;
import algorithms.mergesort.strategies.Strategies;
import algorithms.templates.ThreadedDivideAndConquerable;

import java.util.List;
import java.util.Vector;

public class MergeSortThreadedDNC implements ThreadedDivideAndConquerable<List<Long>> {
    List<Long> input;
    Strategies strategies;

    public MergeSortThreadedDNC(List<Long> input){
        this(input, new Strategies());
    }

    public MergeSortThreadedDNC(List<Long> input, Strategies strategies){
        this.input = input;
        this.strategies = strategies;
    }

    @Override
    public boolean isBasic() {
        return strategies.baseCase().isBaseCase(input);
    }

    @Override
    public List<Long> baseFun() {
        return strategies.baseCase().baseFunction(input);
    }

    @Override
    public List<? extends ThreadedDivideAndConquerable<List<Long>>> decompose() {
        List<MergeSortThreadedDNC> decomposedInput = new Vector<>();
        int middle = input.size() / 2;
        decomposedInput.add(new MergeSortThreadedDNC(input.subList(0, middle)));
        decomposedInput.add(new MergeSortThreadedDNC(input.subList(middle, input.size())));
        return decomposedInput;
    }

    @Override
    public List<Long> recombine(List<List<Long>> intermediateResults) {
        return strategies.recombineStrategy().recombine(intermediateResults);
    }

    @Override
    public boolean isThreadable() {
        return this.input.size() > 20;
    }
}
