package algorithms.mergesort.strategies;

import java.util.List;

public class BaseCaseStrategy {
    public static final BaseCaseStrategy INSTANCE = new BaseCaseStrategy();

    public boolean isBaseCase(List<Long> input) {
        return input.size() < 2;
    }

    public List<Long> baseFunction(List<Long> input) {
        return input;
    }

    @Override
    public String toString(){
        return "BasicBaseCase";
    }
}
