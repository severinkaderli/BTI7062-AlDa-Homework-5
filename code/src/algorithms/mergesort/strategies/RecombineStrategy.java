package algorithms.mergesort.strategies;

import java.util.List;
import java.util.Vector;

public class RecombineStrategy {
    public static final RecombineStrategy INSTANCE = new RecombineStrategy();

    public List<Long> recombine(List<List<Long>> input){
        int leftPtr = 0;
        int rightPtr = 0;
        List<Long> left = input.get(0);
        List<Long> right = input.get(1);
        int size = input.stream().mapToInt(List::size).sum();
        List<Long> recombined = new Vector<>(size);

        while(leftPtr < left.size() && rightPtr < right.size()){
            if(left.get(leftPtr) <= right.get(rightPtr)){
                recombined.add(left.get(leftPtr++));
            }else{
                recombined.add(right.get(rightPtr++));
            }
        }

        while(leftPtr < left.size()){
            recombined.add(left.get(leftPtr++));
        }

        while(rightPtr < right.size()){
            recombined.add(right.get(rightPtr++));
        }

        return recombined;
    }

    @Override
    public String toString(){
        return "BasicRecombination";
    }
}
