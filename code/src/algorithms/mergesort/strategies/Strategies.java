package algorithms.mergesort.strategies;

public class Strategies {
    private BaseCaseStrategy baseCaseStrategy;
    private RecombineStrategy recombineStrategy;

    public Strategies(){
        this(BaseCaseStrategy.INSTANCE, RecombineStrategy.INSTANCE);
    }

    public Strategies(BaseCaseStrategy baseCaseStrategy, RecombineStrategy recombineStrategy){
        this.baseCaseStrategy = baseCaseStrategy;
        this.recombineStrategy = recombineStrategy;
    }

    public BaseCaseStrategy baseCase(){
        return this.baseCaseStrategy;
    }

    public RecombineStrategy recombineStrategy(){
        return this.recombineStrategy;
    }
}
