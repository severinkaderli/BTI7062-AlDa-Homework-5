package algorithms.mergesort.strategies;

import algorithms.utils.InsertionSort;

import java.util.List;

public class InsertionSortBaseCaseStrategy extends BaseCaseStrategy {

    public static final InsertionSortBaseCaseStrategy INSTANCE = new InsertionSortBaseCaseStrategy();

    @Override
    public boolean isBaseCase(List<Long> input) {
        return input.size() <= 10;
    }

    @Override
    public List<Long> baseFunction(List<Long> input) {
        return InsertionSort.sort(input);
    }

    @Override
    public String toString(){
        return "InsertionSortBaseCase";
    }
}
