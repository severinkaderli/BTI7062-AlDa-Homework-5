package algorithms.mergesort.strategies;

import algorithms.utils.InsertionSort;

import java.util.List;
import java.util.Vector;

public class InsertionSortRecombineStrategy extends RecombineStrategy{
    public static final InsertionSortRecombineStrategy INSTANCE = new InsertionSortRecombineStrategy();

    public List<Long> recombine(List<List<Long>> input){
        List<Long> all = new Vector<>(input.get(0));
        all.addAll(input.get(1));
        return InsertionSort.sort(all);
    }

    @Override
    public String toString(){
        return "InsertionSortRecombination";
    }
}
