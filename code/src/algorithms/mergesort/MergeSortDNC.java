package algorithms.mergesort;

import algorithms.mergesort.strategies.BaseCaseStrategy;
import algorithms.mergesort.strategies.Strategies;
import algorithms.templates.DivideAndConquerable;

import java.util.ArrayList;
import java.util.List;

public class MergeSortDNC implements DivideAndConquerable<List<Long>> {
    private final List<Long> input;
    private final Strategies strategies;

    public MergeSortDNC(List<Long> input) {
        this(input, new Strategies());
    }

    public MergeSortDNC(List<Long> input, Strategies strategies){
        this.input = input;
        this.strategies = strategies;
    }

    @Override
    public boolean isBasic() {
        return strategies.baseCase().isBaseCase(input);
    }

    @Override
    public List<Long> baseFun() {
        return strategies.baseCase().baseFunction(input);
    }

    @Override
    public List<? extends DivideAndConquerable<List<Long>>> decompose() {
        List<MergeSortDNC> decomposedInput = new ArrayList<>();
        int middle = input.size() / 2;
        decomposedInput.add(new MergeSortDNC(input.subList(0, middle)));
        decomposedInput.add(new MergeSortDNC(input.subList(middle, input.size())));
        return decomposedInput;
    }

    @Override
    public List<Long> recombine(List<List<Long>> intermediateResults) {
        return strategies.recombineStrategy().recombine(intermediateResults);
    }
}
