package algorithms.utils;

import java.util.List;

public class InsertionSort {

    public static List<Long> sort(List<Long> input){
        if (input.size() < 2) {
            return input;
        }

        for (int i = 1; i < input.size(); i++) {
            int j = i;
            while (j > 0 && input.get(j - 1) > input.get(j)) {
                swap(input, j, --j);
            }
        }
        return input;
    }

    private static void swap(List<Long> array, int a, int b){
        Long swp = array.get(a);
        array.set(b, array.get(a));
        array.set(a, swp);
    }
}
