package algorithms.utils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class DataProvider {
    private static List<Long> all = LongStream.rangeClosed(1L, Config.UPPER_BOUND).boxed().collect(Collectors.toList());
    private static Map<Integer, List<Long>> cache = new HashMap<>(Config.UPPER_BOUND/ Config.STEP);


    static{
        switch (Config.BENCHMARK_CASE){
            case BEST:
                //NOP, already generated best case
                break;
            case AVERAGE:
                if(Config.RANDOM_SEED != -1){
                    Collections.shuffle(all, new Random(Config.RANDOM_SEED));
                }
                Collections.shuffle(all, new Random());
                break;
            case WORST:
                Collections.reverse(all);
                break;
        }
    }

    public static List<Long> get(int amount) {
        return cache.computeIfAbsent(amount, i -> all.subList(0, i));
    }

    public enum Case{
        BEST, AVERAGE, WORST;
    }
}
