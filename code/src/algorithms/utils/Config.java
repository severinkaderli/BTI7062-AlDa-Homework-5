package algorithms.utils;

import algorithms.mergesort.strategies.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Config {
    /**
     * The upper bound of the numbers which will
     * be sorted.
     */
    public static final int UPPER_BOUND = 1_000_000;

    /**
     * Stept to increase numbers to sort by
     */
    public static final int STEP = 50_000;

    /**
     * Number of warmup iterations
     */
    public static final int NUMBER_OF_WARMUP_ITERATIONS = 5;

    /**
     * Strategies to use for the basecase and recombination strategy.
     * BaseCase:
     *      Choose either BaseCase (if input.size less than 2, it's implicitly sorted, return input)
     *    or InsertionSortBaseCase (if input.size less/equal to 10, use InsertionSort and return the sorted list)
     * Recombine:
     *      Choose either RecombineStrategy (The "default mergesort strategy, as described on Wikipedia)
     *    or InsertionSortRecombineStrategy (Appends the subcomponents and uses insertionsort on that list)
     */
    public static final Strategies STRATEGIES = new Strategies(BaseCaseStrategy.INSTANCE, RecombineStrategy.INSTANCE);

    /**
     * The thread counts which should be used for the calculations.
     * The application will sequentially test with the threadcounts given here.
     */
    public static final List<Integer> THREAD_COUNTS = new ArrayList<>(Arrays.asList(2, 4, 6));

    /**
     * The case for which we should generate Data [BEST, AVERAGE, WORST].
     * Best:    the list will already be sorted
     * Average: the list is shuffled randomly
     * Worst:   the list is sorted backwards
     */
    public static final DataProvider.Case BENCHMARK_CASE = DataProvider.Case.WORST;

    /**
     * The seed to use for any randomness.
     * Set to -1 for using using unparameterized random
     */
    public static final int RANDOM_SEED = 1337;

    /**
     * Whether to apply smoothing or not.
     * Smoothes out the chart.
     */
    public static final boolean APPLY_SMOOTHING = true;

    /**
     * Values to use for smoothing
     * a factor of 5 will use the average of this + the last 5 values as the datapoint
     */
    public static final int SMOOTHING_FACTOR = 5;

    /**
     * Checks the generated results for correctness.
     * WARNING! This has a performance impact and measurements are invalid.
     */
    public static final boolean CHECK_CORRECTNESS = false;
}
