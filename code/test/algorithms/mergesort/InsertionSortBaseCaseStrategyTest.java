package algorithms.mergesort;

import algorithms.mergesort.strategies.InsertionSortBaseCaseStrategy;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class InsertionSortBaseCaseStrategyTest {

    @Test
    public void baseFunction_whenGivenInput_returnsInputSorted() {
        test(new Integer[]{1, 3, 4, 4 ,5 ,6, 2, 4, 3, 4});
        test(new Integer[]{1, -3, 4, 4 ,-5 ,6, 2, -4, 3, 4});
        test(new Integer[]{1, 1, 1, 1, 10000});
        test(new Integer[]{2, 1});
        test(new Integer[]{2});
    }

    private void test(Integer[] values){
        List<Long> input = Stream.of(values).mapToLong(Integer::longValue).boxed().collect(Collectors.toList());
        assertSorted(InsertionSortBaseCaseStrategy.INSTANCE.baseFunction(input));
    }

    private void assertSorted(List<Long> actual) {
        List<Long> expected = new ArrayList<>(actual);
        Collections.sort(expected);
        assertEquals(expected, actual);
    }
}
